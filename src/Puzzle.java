import java.util.*;

/* Mõtteid ning koodi-ideid: https://bitbucket.org/Sgreef/home7/src/740b73737e8de41e4b16ce2c7b4a9b4063e1247f/src/Puzzle.java?
 * at=master&fileviewer=file-view-default
 * http://www.kosbie.net/cmu/fall-09/15-110/handouts/recursion/Cryptarithms.java
 * https://programmingpraxis.com/2012/07/31/send-more-money-part-1/
 * http://stackoverflow.com/questions/17893193/java-program-to-solve-simple-cryptarithmetic-puzzle
 */

class Puzzle {

   public static void permute(int n, int k) {
      permute(n, k, new HashSet<>(), new int[k]);
   }

   public static void permute(int n, int k, HashSet<Integer> set, int[] permutation) {
      if (set.size() == k)
         doPermute(n, k, set, permutation);
      else {
         for (int i=0; i<n; i++)
            if (!set.contains(i)) {
               permutation[set.size()] = i;
               set.add(i);
               permute(n,k,set,permutation);
               set.remove(i);
            }
      }
   }

   public static void doPermute(int n, int k, HashSet<Integer> set, int[] permutatsioon) {

      HashMap<Character,Integer> characterMap = new HashMap<>();
      for (int i = 0; i < k; i++)
         characterMap.put(letterid.charAt(i), permutatsioon[i]);
      int[] values = new int[3];
      for (int j = 0; j < 3; j++) {
         String word = words[j];
         if (characterMap.get(word.charAt(0)) == 0)
            return;
         int value = 0;
         for (int i = 0; i < word.length(); i++)
            value = 10 * value + characterMap.get(word.charAt(i));
         values[j] = value;
      }

      if (values[0] + values[1] == values[2]) {
         System.out.println(characterMap);
         System.out.format("%8d\n+%7d\n=%7d\n",values[0],values[1],values[2]);
         loend++;
      }
   }

   private static String letterid;
   private static String[] words;
   private static int loend = 0;

   public static void main(String[] args) {
      if(args.length != 3){
         throw new RuntimeException("Parameetriteks peab olema kolm stringi, kuid hetkel on " +
                 args.length);
      }
      for (String s : args){
         if(s.length() > 18)
            throw new RuntimeException("Sõne pikkus tohib olla kuni 18 tähte: " + s);
         if(s.length() <= 0)
            throw new RuntimeException("Parameetreid pole: " + s);
         for (char c : s.toCharArray()){
            if(!Character.isLetter(c))
               throw new RuntimeException("Sõnes ei ole ainult tähed: " + s);
         }
      }
      words = new String[3];
      words[0] = args[0];
      words[1] = args[1];
      words[2] = args[2];
      String all = args[0] + args[1] + args[2];
      letterid = "";
      for (int i = 0; i < all.length(); i++) {
         char c = all.charAt(i);
         if (letterid.indexOf(c) < 0)
            letterid += c;
      }
      permute(10, letterid.length());
      System.out.println("V6imalike lahendite arv: " + loend);
   }
}